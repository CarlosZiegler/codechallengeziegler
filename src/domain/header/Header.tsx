/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { FC, useContext } from 'react'
import { makeStyles, AppBar } from '@material-ui/core'
import { themeValues } from 'app/themes/themeValues'
import Tab from '../tab/Tab'
import ProjectContext from '../project/ProjectContext'

const useStyles = makeStyles({
	root: {
		height: themeValues().sizes.TabHeader.height,
		background: themeValues().gradients.background,
		position: 'relative',
		color: 'white',
	},
})

// eslint-disable-next-line no-unused-vars
const Header: FC = props => {
	const { allProjects } = useContext(ProjectContext)

	const classes = useStyles()
	// Send a projects to Tab Component
	return (
		<div className={classes.root}>
			<AppBar position="static">
				<Tab projects={allProjects} />
			</AppBar>
		</div>
	)
}

export default Header
