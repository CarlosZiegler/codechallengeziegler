/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable react/no-unescaped-entities */
import React, { useState } from 'react'
import { makeStyles, Theme } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import { Link } from 'react-router-dom'
import Content from '../content/Content'

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
function a11yProps(index: any) {
	return {
		id: `simple-tab-${index}`,
		'aria-controls': `simple-tabpanel-${index}`,
	}
}

// eslint-disable-next-line no-unused-vars
const useStyles = makeStyles((theme: Theme) => ({
	root: {
		flexGrow: 1,
		marginTop: 100,
	},
}))

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export default function SimpleTabs(props: any) {
	// Projects from API
	const { projects } = props

	const classes = useStyles()
	// set index for a actived Tab
	const [value, setValue] = useState(0)

	// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
	const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
		setValue(newValue)
	}

	return (
		<div className={classes.root}>
			<AppBar position="static">
				<Tabs
					value={value}
					onChange={handleChange}
					aria-label="simple tabs example"
				>
					>
					{/** For each in a Array projects and return a new Tab with Label and data from Project */}
					{projects.map((project: any) => {
						if (project === null) {
							return null
						}
						return (
							<Tab
								component={Link}
								to={`/projects/${project.id}`}
								key={project.id}
								label={project.name}
								{...a11yProps(project.id)}
							/>
						)
					})}
				</Tabs>

				<Content projectsArray={projects} tabActive={value} />
			</AppBar>
		</div>
	)
}
