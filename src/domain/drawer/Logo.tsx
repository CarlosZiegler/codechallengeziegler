import React, { FC } from 'react'
import { Link } from 'react-router-dom'

import { makeStyles } from '@material-ui/core'

// get url from a Image
const logoTendence = {
	url: '/images/logo.png',
}
export interface LogoProps {
	invert?: boolean
	marginTop?: number
	marginBottom?: number
}

const useStyles = makeStyles(() => ({
	logo: {
		width: '100%',
		padding: '30px',
	},
}))

const Logo: FC<LogoProps> = () => {
	const classes = useStyles()

	return (
		<Link to="/">
			<img src={logoTendence.url} className={classes.logo} alt="Logo" />
		</Link>
	)
}

export default Logo
