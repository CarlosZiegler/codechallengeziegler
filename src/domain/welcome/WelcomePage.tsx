import React, { FC } from 'react'
import { makeStyles, Theme } from '@material-ui/core'

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		padding: theme.spacing(4),
	},
}))

const WelcomePage: FC = ({ children }) => {
	const classes = useStyles()
	return <div className={classes.root}>{children}</div>
}

export default WelcomePage
