/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-alert */
import React, { useContext } from 'react'
import { Typography } from '@material-ui/core'
import CustomButton from '../../component/CustomButton'

import ProjectContext from '../project/ProjectContext'

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export default function Content(props: any) {
	// active Tab
	const { tabActive } = props
	// useContext for a Projects
	const { allProjects } = useContext(ProjectContext)

	return (
		<div>
			{allProjects.map((pro: any, index: number) => {
				if (index === tabActive) {
					return (
						<div key={pro.id}>
							<Typography variant="h3" component="h1" color="textPrimary">
								{pro.name}
							</Typography>
							<CustomButton
								color="primary"
								disabled
								text="Button Blue"
								onClick={() => {
									// eslint-disable-next-line no-alert
									alert('Function Blue finished')
								}}
							/>
							<CustomButton
								color="secondary"
								disabled
								text="Button Green"
								onClick={() => {
									alert('Function Green finished')
								}}
							/>
							<CustomButton
								color="inherit"
								disabled
								text="Button Red"
								onClick={() => {
									alert('Function Red finished')
								}}
							/>
						</div>
					)
				}
				return null
			})}
		</div>
	)
}
