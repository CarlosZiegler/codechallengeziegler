/* eslint-disable @typescript-eslint/no-explicit-any */
import { createContext } from 'react'

// create context for a Projects
const ProjectContext = createContext({
	allProjects: [null],
	activeProject: 0,
})

export default ProjectContext
