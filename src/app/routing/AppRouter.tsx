import React, { ReactElement } from 'react'
import { Switch, BrowserRouter } from 'react-router-dom'
import { routes } from 'app/routing/routes'
import TemplateProject from '../../template/TemplateProject'
import Error404 from '../../template/404'
import NotFound from '../../domain/notFound/NotFound'
import ProjectPage from '../../domain/project/ProjectPage'

const AppRouter = (): ReactElement => {
	return (
		<BrowserRouter>
			<Switch>
				<TemplateProject exact path={routes.home} component={ProjectPage} />
				<TemplateProject exact path={routes.projects} component={ProjectPage} />
				<Error404 path="*" component={NotFound} />
			</Switch>
		</BrowserRouter>
	)
}

export default AppRouter
