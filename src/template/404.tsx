import React, { FC, FunctionComponent } from 'react'
import { RouteProps, Route, Link } from 'react-router-dom'

// URL and alt-text for a Image
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const PageNotFound: any = {
	url: '/images/page_not_found.png',
	alt: ' PagenotFound',
}

export interface TemplateNotFound404 extends RouteProps {
	component: FunctionComponent
}

// Template Error404 with a Image and link for back to home
const Error404: FC<TemplateNotFound404> = () => {
	return (
		<Route
			render={() => (
				<>
					<img src={PageNotFound.url} width="100%" alt={PageNotFound.alt} />
					<h3 style={{ textAlign: 'center' }}>
						<Link to="/">Go to Home ... </Link>
					</h3>
				</>
			)}
		/>
	)
}

export default Error404
