/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { FC, FunctionComponent, useEffect, useState } from 'react'
import { RouteProps, Route } from 'react-router-dom'
import { Paper, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import gql from 'graphql-tag'
import { themeValues } from '../app/themes/themeValues'
import DrawerNavigation from '../domain/drawer/DrawerNavigation'
import Header from '../domain/header/Header'
import ProjectContext from '../domain/project/ProjectContext'
import client from '../app/services/apiClientGraphQL'

export interface TemplateWelcomeProps extends RouteProps {
	component: FunctionComponent
}

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		display: 'flex',
		minHeight: '100vh',
	},
	contentContainer: {
		background: theme.palette.primary.main,
		width: `calc(100% - ${themeValues().sizes.Drawer.width}px)`,
		zIndex: 1200,
		[theme.breakpoints.down('sm')]: {
			width: '100%',
		},
	},
	pageContainer: {
		background: theme.palette.common.white,
		minHeight: `calc(100vh - ${themeValues().sizes.TabHeader.height}px)`,
		borderRadius: 0,
		width: `calc(100% + ${themeValues().mainOverlap}px)`,
		borderTopLeftRadius: 4,
		marginLeft: -themeValues().mainOverlap,
		[theme.breakpoints.down('sm')]: {
			width: '100%',
			borderTopLeftRadius: 0,
			marginLeft: 0,
		},
	},
	pageContent: {
		width: '100%',
	},
}))

const TemplateProject: FC<TemplateWelcomeProps> = ({
	component: Component,
	...rest
}) => {
	const classes = useStyles()
	// set state
	const [arrayprojects, setArrayprojects] = useState([])

	useEffect(() => {
		// get AllProjects from Api with a Query and set State
		// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
		async function getData() {
			const response = await client.query({
				query: gql`
					query GetProjects {
						allProjects {
							name
							id
						}
					}
				`,
			})

			setArrayprojects(response.data.allProjects)
		}
		getData()
	}, [])

	return (
		<Route
			{...rest}
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			// eslint-disable-next-line no-unused-vars
			render={props => (
				<div className={classes.root}>
					<ProjectContext.Provider
						value={{
							allProjects: arrayprojects,
							activeProject: 0,
						}}
					>
						<DrawerNavigation />
						<div className={classes.contentContainer}>
							<Header />

							<Paper elevation={3} className={classes.pageContainer}>
								<div className={classes.pageContent}>
									<Component />
								</div>
							</Paper>
						</div>
					</ProjectContext.Provider>
				</div>
			)}
		/>
	)
}

export default TemplateProject
