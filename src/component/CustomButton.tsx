/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React from 'react'
import clsx from 'clsx'
import {
	createStyles,
	makeStyles,
	Theme,
	createMuiTheme,
	ThemeProvider,
} from '@material-ui/core/styles'
import CircularProgress from '@material-ui/core/CircularProgress'
import { green, blue, red } from '@material-ui/core/colors'
import Button from '@material-ui/core/Button'

// provide a styles for a loading button
const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		root: {
			display: 'flex',
			alignItems: 'center',
		},
		wrapper: {
			margin: theme.spacing(1),
			position: 'relative',
		},
		buttonSuccess: {
			backgroundColor: green[500],
			'&:hover': {
				backgroundColor: green[700],
			},
		},
		fabProgress: {
			color: green[500],
			position: 'absolute',
			top: -6,
			left: -6,
			zIndex: 1,
		},
		buttonProgress: {
			color: green[500],
			position: 'absolute',
			top: '50%',
			left: '50%',
			marginTop: -12,
			marginLeft: -12,
		},
		buttonRed: {
			color: red[500],
			position: 'absolute',
			top: '50%',
			left: '50%',
			marginTop: -12,
			marginLeft: -12,
		},
	})
)

// changed color for a buttons
const theme = createMuiTheme({
	palette: {
		primary: blue,
		secondary: green,
	},
})

// create a type Custombutton for a Dinamic Text
export type CustomButtonProps = {
	text?: string
}

export default function CircularIntegration<CustomButtonProps>(props: any) {
	// change a color, text and a callback for a Onclick function
	const { color, text, onClick } = props
	const classes = useStyles()

	// set state for a action Button (Loading and a sucess)
	const [loading, setLoading] = React.useState(false)
	const [success, setSuccess] = React.useState(false)
	const timer = React.useRef<number>()

	const buttonClassname = clsx({
		[classes.buttonSuccess]: success,
	})

	React.useEffect(() => {
		return () => {
			clearTimeout(timer.current)
		}
	}, [])

	const handleButtonClick = () => {
		if (!loading) {
			setSuccess(false)
			setLoading(true)
			timer.current = window.setTimeout(() => {
				setSuccess(true)
				setLoading(false)
				onClick()
			}, 2000)
		}
	}

	return (
		<div className={classes.root}>
			<div className={classes.wrapper}>
				<ThemeProvider theme={theme}>
					<Button
						variant="contained"
						color={color}
						className={buttonClassname}
						disabled={loading}
						onClick={handleButtonClick}
					>
						{text}
					</Button>
					{loading && (
						<CircularProgress size={24} className={classes.buttonProgress} />
					)}
				</ThemeProvider>
			</div>
		</div>
	)
}
