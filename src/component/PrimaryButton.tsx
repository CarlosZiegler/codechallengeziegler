import React, { FC } from 'react'
import { Button } from '@material-ui/core'

export type PrimaryButtonProps = {
	disabled?: boolean
	variant?: 'text' | 'contained' | 'outlined' | undefined
	color?: 'inherit' | 'primary' | 'secondary' | 'default' | undefined
	text?: string
	onClick?: Function
}

// variant per props
const PrimaryButton: FC<PrimaryButtonProps> = props => {
	const { disabled, variant, color, text } = props

	return (
		<Button
			// eslint-disable-next-line @typescript-eslint/no-empty-function
			onClick={() => {}}
			disabled={disabled}
			variant={variant}
			color={color}
		>
			{text}
		</Button>
	)
}

export default PrimaryButton
